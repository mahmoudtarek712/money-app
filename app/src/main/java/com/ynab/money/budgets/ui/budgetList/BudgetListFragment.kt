package com.ynab.money.budgets.ui.budgetList

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ynab.money.R
import com.ynab.money.budgets.model.Budget
import com.ynab.money.budgets.model.BudgetsModel
import com.ynab.money.common.BusinessConstants.Companion.BUDGET_ID
import com.ynab.money.common.data.MainModels.BaseDataWrapper
import com.ynab.money.common.views.AutoClearedValue
import com.ynab.money.common.views.autoCleared
import com.ynab.money.databinding.FragmentBudgetListBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BudgetListFragment : Fragment(), KodeinAware, BudgetListAdapter.BudgetItemListener {
    override val kodein by kodein()
    private var binding : FragmentBudgetListBinding by autoCleared()
    private val factory: BudgetViewModelFactory by instance()
    private val viewModel by activityViewModels<BudgetViewModel>() { factory }
    private lateinit var adapter: BudgetListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBudgetListBinding.inflate(inflater, container, false)
        setupRecyclerView()
        initObserver()
        return binding.root
    }

    private fun initObserver() {
        val observer = Observer<BaseDataWrapper<BudgetsModel>> {
            Log.e("Data" , it.data.toString())
            if (it.data != null) {
                adapter.setItems(it.data.budgets as ArrayList<Budget>)
            }
        }
        viewModel.userLogin().observe(viewLifecycleOwner, observer)
    }

    private fun setupRecyclerView() {
        adapter = BudgetListAdapter(this)
        binding.budgetsRv.layoutManager = LinearLayoutManager(requireContext())
        binding.budgetsRv.adapter = adapter
    }

    override fun onClickedBudget(budgetId: String) {
        findNavController().navigate(
            R.id.action_budgetListFragment_to_budgetDetailFragment,
            bundleOf(BUDGET_ID to budgetId)
        )
    }
}