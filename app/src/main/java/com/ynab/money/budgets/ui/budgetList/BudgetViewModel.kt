package com.ynab.money.budgets.ui.budgetList

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.ynab.money.budgets.model.Budget
import com.ynab.money.budgets.model.BudgetsModel
import com.ynab.money.common.data.MainModels.BaseDataWrapper
import com.ynab.money.common.data.RemoteDataSource
import com.ynab.money.common.data.ResultWrapper
import com.ynab.money.common.managers.Coroutines
import com.ynab.money.common.managers.MainAppViewModel
import com.ynab.money.common.managers.SingleLiveEvent

class BudgetViewModel (var remoteDataSource: RemoteDataSource) : MainAppViewModel() {

    private lateinit var getBudgetList : MutableLiveData<BaseDataWrapper<BudgetsModel>>


    fun userLogin() : MutableLiveData<BaseDataWrapper<BudgetsModel>> {
        getBudgetList = MutableLiveData()
        Coroutines.io {
            validateResponse(remoteDataSource.getBudgets(), getBudgetList)
        }
        return getBudgetList
    }

}