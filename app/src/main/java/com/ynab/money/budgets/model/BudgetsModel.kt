package com.ynab.money.budgets.model

//data class BudgetsModel(
//    val `data`: Data
//)

data class BudgetsModel(
    val budgets: List<Budget>,
    val default_budget: Any
)

data class Budget(
    val currency_format: CurrencyFormat,
    val date_format: DateFormat,
    val first_month: String,
    val id: String,
    val last_modified_on: String,
    val last_month: String,
    val name: String
)

data class CurrencyFormat(
    val currency_symbol: String,
    val decimal_digits: Int,
    val decimal_separator: String,
    val display_symbol: Boolean,
    val example_format: String,
    val group_separator: String,
    val iso_code: String,
    val symbol_first: Boolean
)

data class DateFormat(
    val format: String
)