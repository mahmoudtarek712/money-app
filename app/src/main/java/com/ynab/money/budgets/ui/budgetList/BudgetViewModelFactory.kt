package com.ynab.money.budgets.ui.budgetList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ynab.money.common.data.RemoteDataSource

@Suppress("UNCHECKED_CAST")
class BudgetViewModelFactory( private val remoteDataSource: RemoteDataSource) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BudgetViewModel(remoteDataSource) as T
    }
}