package com.ynab.money.budgets.ui.budgetList

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ynab.money.budgets.model.Budget
import com.ynab.money.databinding.BudgetItemBinding
import kotlin.collections.ArrayList

class BudgetListAdapter (private val listener: BudgetItemListener) : RecyclerView.Adapter<BudgetViewHolder>() {

    interface BudgetItemListener {
        fun onClickedBudget(budgetId: String)
    }

    private val items = ArrayList<Budget>()

    fun setItems(items: ArrayList<Budget>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BudgetViewHolder {
        val binding: BudgetItemBinding = BudgetItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BudgetViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BudgetViewHolder, position: Int) = holder.bind(items[position])
}

class BudgetViewHolder(private val itemBinding: BudgetItemBinding, private val listener: BudgetListAdapter.BudgetItemListener) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var budget: Budget

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: Budget) {
        this.budget = item
        itemBinding.budgetNameTV.text = item.name
        itemBinding.budgetLastUsetTV.text = item.last_modified_on
    }

    override fun onClick(v: View?) {
        listener.onClickedBudget(budget.id)
    }

}

