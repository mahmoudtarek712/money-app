package com.ynab.money.common.data

import com.ynab.money.common.data.MainModels.ErrorResponse


sealed class ResultWrapper<out T> {
    data class Success<out T>(val value: T) : ResultWrapper<T>()
    data class GenericError(val code: Int? = null, val error: ErrorResponse? = null) : ResultWrapper<Nothing>()
    object NetworkError : ResultWrapper<Nothing>()
    object ServerError : ResultWrapper<Nothing>()
    object AuthorizationError : ResultWrapper<Nothing>()
    object Loading : ResultWrapper<Nothing>()
}