package com.ynab.money.common.data

import com.google.gson.Gson
import com.ynab.money.common.data.MainModels.BaseDataWrapper
import com.ynab.money.common.data.MainModels.ErrorResponse
import com.ynab.money.common.data.MainModels.Status
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

abstract class SafeApiRequest {

    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): ResultWrapper<T> {
        try {
            val response = call.invoke()
            return if (response.code().equals(Status.SUCCESS.status)) {
                var responseData = response.body() as BaseDataWrapper<Any>
                if (responseData.data !=null) {
                    ResultWrapper.Success(response.body()!!)
                } else {
                    if (responseData.code!! != Status.AUTHORIZATION.status) {
                        var errorResponse = ErrorResponse(responseData.message)
                        ResultWrapper.GenericError(responseData.code, errorResponse)
                    } else {
                        ResultWrapper.AuthorizationError
                    }

                }
            } else if (response.code().equals(Status.AUTHORIZATION.status)) {
                ResultWrapper.AuthorizationError
            }else {
                ResultWrapper.ServerError
            }
        } catch (throwable: Throwable) {
            return when (throwable) {
                is IOException -> ResultWrapper.NetworkError
                is HttpException -> {
                    val code = throwable.code()
                    if (code != Status.AUTHORIZATION.status) {
                        val errorResponse = convertErrorBody(throwable)
                        ResultWrapper.GenericError(code, errorResponse)
                    } else {
                        ResultWrapper.AuthorizationError
                    }
                }
                else -> {
                    ResultWrapper.GenericError(null, null)
                }
            }
        }

    }

    private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
        return try {
            throwable.response()?.errorBody()?.charStream()?.let {
                Gson().fromJson(it, ErrorResponse::class.java)

            }
        } catch (exception: Exception) {
            null
        }
    }
}
