package com.ynab.money.common.managers

import android.content.Context
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.emptyPreferences
import androidx.datastore.preferences.preferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException


enum class Auth {
    LOGGEDIN, GUEST
}

class PreferencesManager (private val context: Context){
    private val dataStore = context.createDataStore(name = "money_app_pref")

    val authFlow: Flow<Auth> = dataStore.data
        .catch {
            if (it is IOException) {
                it.printStackTrace()
                emit(emptyPreferences())
            } else {
                throw it
            }
        }
        .map { preference ->
            val isLoggedIn = preference[IS_AUTHORIZED] ?: false
            when (isLoggedIn) {
                true -> Auth.LOGGEDIN
                false -> Auth.GUEST
            }
        }

    suspend fun setIsLoggedIn(auth: Auth) {
        dataStore.edit { preferences ->
            preferences[IS_AUTHORIZED] = when (auth) {
                Auth.GUEST -> false
                Auth.LOGGEDIN -> true
            }
        }
    }

    companion object {
        val IS_AUTHORIZED = preferencesKey<Boolean>("isLoggedIn")
    }
}