package com.ynab.money.common

class BusinessConstants {
    companion object{
        const val BASE_URL = "https://api.youneedabudget.com/v1/"
        const val USER_TOKEN = "Bearer 4c5284f610ea5604802ed8871e6f007cbb2e8512a8e3f4cffcfe3539a8857e25"
        const val BUDGET_ID = "budget_id"
    }
}
