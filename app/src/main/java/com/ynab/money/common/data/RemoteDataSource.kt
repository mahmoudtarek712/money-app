package com.ynab.money.common.data

import com.ynab.money.budgets.model.BudgetsModel
import com.ynab.money.common.data.MainModels.BaseDataWrapper


class RemoteDataSource(private val apiService: APIsService) : SafeApiRequest(){


    suspend fun getBudgets(): ResultWrapper<BaseDataWrapper<BudgetsModel>> {
        return apiRequest { apiService.getMyBudget() }
    }
}