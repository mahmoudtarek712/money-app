package com.ynab.money.common.data.MainModels

data class BaseDataWrapper<T>(
    val message: String,
    val status: Boolean,
    val `data`: T?,
    var code: Int?
)