package com.ynab.money.common.data


import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.ynab.money.budgets.model.BudgetsModel
import com.ynab.money.common.BusinessConstants.Companion.BASE_URL
import com.ynab.money.common.data.MainModels.BaseDataWrapper
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*
import java.util.concurrent.TimeUnit

interface APIsService {

    @GET("budgets")
    suspend fun getMyBudget(): Response<BaseDataWrapper<BudgetsModel>>


    companion object {
        fun Retrofit(networkConnectionInterceptor: NetworkConnectionInterceptor): Retrofit {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okkHttpclient = OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES) // write timeout
                .readTimeout(2, TimeUnit.MINUTES)
                .addInterceptor(logging)
                .addInterceptor(networkConnectionInterceptor)
                .connectionSpecs(
                    Arrays.asList(
                        ConnectionSpec.MODERN_TLS,
                        ConnectionSpec.COMPATIBLE_TLS,
                        ConnectionSpec.CLEARTEXT
                    )
                )
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .cache(null)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

        }
        operator fun invoke(retrofit: Retrofit): APIsService {
            return retrofit
                .create(APIsService::class.java)
        }

    }
}