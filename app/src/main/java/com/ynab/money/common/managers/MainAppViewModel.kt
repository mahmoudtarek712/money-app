package com.ynab.money.common.managers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ynab.money.common.data.ResultWrapper

open class MainAppViewModel: ViewModel() {

    protected val _networkError: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var networkError: LiveData<Boolean> = _networkError

    protected val _serverError: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var serverError: LiveData<Boolean> = _serverError

    protected val _authorizationError: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var authorizationError: LiveData<Boolean> = _authorizationError

    protected val _errorMessage: SingleLiveEvent<String> = SingleLiveEvent()
    var errorMessage: LiveData<String> = _errorMessage


    fun <T> validateResponse(response: ResultWrapper<T>, source: MutableLiveData<T>) {
        when (response) {
            is ResultWrapper.NetworkError -> _networkError.postValue(true)
            is ResultWrapper.ServerError -> _serverError.postValue(true)
            is ResultWrapper.AuthorizationError -> _authorizationError.postValue(true)
            is ResultWrapper.GenericError -> response.error?.message?.let {
                _errorMessage.postValue(it)
            }
            is ResultWrapper.Success -> {
                source.postValue(response.value)
            }
        }

    }


    fun <T> validateResponse(response: ResultWrapper<T>): T? {
        when (response) {
            is ResultWrapper.NetworkError -> _networkError.postValue(true)
            is ResultWrapper.GenericError -> response.error?.message?.let {
                _errorMessage.postValue(it)
            }
            is ResultWrapper.AuthorizationError -> _authorizationError.postValue(true)
            is ResultWrapper.ServerError -> _serverError.postValue(true)
            is ResultWrapper.Success -> {
                return response.value
            }
        }
        return null

    }

    fun mergeViewModelSources(vararg sources: MainAppViewModel) {
        val sourceNetworkError: ArrayList<LiveData<Boolean>> = ArrayList()
        val sourceErrorMessage: ArrayList<LiveData<String>> = ArrayList()

        for (source in sources) {
            sourceNetworkError.add(source.networkError)
            sourceErrorMessage.add(source.errorMessage)
        }
        // add this view model networ error and error meesage
        sourceNetworkError.add(networkError)
        sourceErrorMessage.add(errorMessage)

        //compain all live data response
        errorMessage = mergeDataSources(sourceErrorMessage)
        networkError = mergeDataSources(sourceNetworkError)


    }

    private fun <T> mergeDataSources(sources: List<LiveData<T>>): LiveData<T> {
        val mergedSources: MediatorLiveData<T> = MediatorLiveData()
        for (source in sources) {
            mergedSources.addSource(source, { value -> mergedSources.setValue(value) })
        }
        return mergedSources
    }
}