package com.ynab.money

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import com.ynab.money.budgets.ui.budgetList.BudgetViewModelFactory
import com.ynab.money.common.data.APIsService
import com.ynab.money.common.data.APIsService.Companion.Retrofit
import com.ynab.money.common.data.NetworkConnectionInterceptor
import com.ynab.money.common.data.RemoteDataSource
import com.ynab.money.common.managers.PreferencesManager
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class App : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@App))
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { Retrofit(instance()) }
        bind() from provider { APIsService(instance()) }
        bind() from provider { BudgetViewModelFactory(instance()) }
        bind() from provider { RemoteDataSource(instance()) }
        bind() from provider { PreferencesManager(instance()) }
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }


    companion object {
        private lateinit var appContext: Context
    }
}